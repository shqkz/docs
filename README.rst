**************************
Интеграция с системой ЕИС ЖКХ
**************************
 Все запросы необходимо отправлять на адрес shanyraq@dbt.asia По ошибкам прилагать обязательно скриншоты. Ответ на ваш запрос поступит в порядке очереди. Длительность ожидания зависит от сложности задачи(направленной заявки).
 
Значения сокращений в системе E-Shanyraq:

ЕИС ЖФ и ЖКХ - Единая информационная система жилищного фонда и коммунального хозяйства

МЖД	- Многоквартирный жилой дом

ОСИ	- Объединение собственников имущества

КСК	- Кооператив собственников квартир

УК	- Управляющая компания

СК	- Сервисная компания

МИО	- Местный исполнительный орган

СЕМ	- Субъект естественной монополии

ПУ	- Прибор учета

ОПУ	- Общедомовой прибор учета

ИПУ	- Индивидуальный прибор учета

ФИО	- Фамилия, имя, отчество

ПО	- Программное обеспечение

ЖКХ	- Жилищно-коммунальное хозяйство

КАТО - Классификатор административно-территориальных объектов



Пример загрузки данных с помощью CURL

Примечание: Value - это показания счетчика, его необходимо тоже указывать. Счетчик необходим для того чтобы считать показание. Например, счетчик показывает количество тепла за весь накопленный период времени.

ОТВЕТЫ на ваши запросы будут предоставляться в порядке очереди и продолжительность ожидания в зависимости от сложности ошибки, вопроса. 

Узнайте ИД вашей организаций:


.. code:: bash

    curl -X GET 'https://shanyraq.valis.kz/api/res.users?fields=id,name,company_id' -H 'api_key: ВАШ-КЛЮЧ'

Ответ:

.. code:: JSON

    {"records":[{"id":82,"name":"Сидоров Сергей Владимирович","company_id":[74,"ТОО Счетчики"]}],"length":1}

Узнайте Ваш ИД КАТО (Классификатор Административно-Территориальных Объектов):


.. code:: bash

    curl -X GET 'https://shanyraq.valis.kz/api/kato.kato?name=Астана' -H 'api_key: ВАШ-КЛЮЧ'

Ответ:

.. code:: JSON

    {"records": [{"code": "710000000", "id": 16788, "name": "\u0433.\u0410\u0441\u0442\u0430\u043d\u0430"}], "length": 1}

Загрузка МЖД


.. code:: bash

    curl -X POST https://shanyraq.valis.kz/api/property.building -d '{"street": "Абылай Хана", "street_no":"3", "kato": 16788}' -H 'Content-type: application/json' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"jsonrpc": "2.0", "id": null, "result": {"id": 175}}

Просмотр Справочника типов прибора


.. code:: bash

    curl -X GET https://shanyraq.valis.kz/api/property.meter.type -H 'api_key: ВАШ-КЛЮЧ'
Загрузка Приборов Учета


.. code:: bash

    curl -X POST https://shanyraq.valis.kz/api/property.meter -d '{"category_id": "opu","type_id":5, "building_id":175, "serial": "SN1231243523"}' -H 'Content-type: application/json' -H 'api_key: ВАШ-КЛЮЧ'

Ответ:

.. code:: JSON

    {"jsonrpc": "2.0", "id": null, "result": {"id": 1455}}


Загрузка Журнала приборов учета

.. code:: bash

    curl -X POST https://shanyraq.valis.kz/api/property.meter.log -d '{"meter_id": 1455, "building_id":175, "check_date":"2018-01-01 14:02:33", "value": 124234.32, "t_in": 56.585445404052734, "t_out": 42.502155303955078, "t_delta": 14.083290100097656, "q_in": 5.0980558395385742, "q_out": 3.5513300895690918, "q_delta": 1.5467253923416138, "v_in": 100.291015625,"v_out": 95.1399917602539,"v_delta": 5.1510238647460938}' -H 'Content-type: application/json' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"jsonrpc": "2.0", "id": null, "result": {"id": 123534}}

.. code:: bash

    curl -X POST https://shanyraq.valis.kz/api/property.meter.log -d '{"meter_id": 1455, "building_id":175,"check_date":"2018-02-01 16:20:47", "value": 124345.61, "t_in": 56.585445404052734, "t_out": 42.502155303955078, "t_delta": 14.083290100097656, "q_in": 5.0980558395385742, "q_out": 3.5513300895690918, "q_delta": 1.5467253923416138, "v_in": 100.291015625,"v_out": 95.1399917602539,"v_delta": 5.1510238647460938}' -H 'Content-type: application/json' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"jsonrpc": "2.0", "id": null, "result": {"id": 123535}}

Загрузка Квартир


.. code:: bash

    curl -X POST https://shanyraq.valis.kz/api/res.partner -d '{"name": "Иванов Иван Иванович"}' -H 'Content-type: application/json' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"jsonrpc": "2.0", "id": null, "result": {"id": 13433}}

.. code:: bash

    curl -X POST https://shanyraq.valis.kz/api/property.apartment -d '{"name": "Квартира 1","number":1, "owner_id":13433, "building_id":175}' -H 'Content-type: application/json' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"jsonrpc": "2.0", "id": null, "result": {"id": 6433}}
    
    
Если хотите много данных 1 запросом отправить есть метод загрузки нескольких записей
Ниже пример загрузки показание прибора (property.meter.log) за 2 дня одним запросом


.. code:: bash

    curl -X POST -H "Content-type:application/json" https://shanyraq.valis.kz/api/batch/property.meter.log/ -d '{"records":[{"meter_id":1455, "building_id":175, "check_date":"2018-01-01", "value": 124345.61, "t_in": 56.585445404052734, "t_out": 42.502155303955078, "t_delta": 14.083290100097656, "q_in": 5.0980558395385742, "q_out": 3.5513300895690918, "q_delta": 1.5467253923416138, "v_in": 100.291015625,"v_out": 95.1399917602539,"v_delta": 5.1510238647460938},{"meter_id":1455, "building_id":175, "check_date":"2018-01-02", "value": 125688.84, "t_in": 56.585445404052734, "t_out": 42.502155303955078, "t_delta": 14.083290100097656, "q_in": 5.0980558395385742, "q_out": 3.5513300895690918, "q_delta": 1.5467253923416138, "v_in": 100.291015625,"v_out": 95.1399917602539,"v_delta": 5.1510238647460938}]}' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"jsonrpc": "2.0", "id": null, "result": {"id": [12562, 12563]}}

 
Если хотите поискать МЖД или Показание можете в запрос можно передавать искомое имя или другое поле
Ниже пример поиска МЖД (property.building) с Названием улице Юбилейный


.. code:: bash

    curl -X GET 'https://shanyraq.valis.kz/api/property.building?fields=id,name&street=Юбилейный' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"records":[{"id":225,"name":"мкр. Юбилейный 14"}, ... ],"length":22}

Такой же пример поиска по КАТО города Костанай


.. code:: bash

    curl -X GET 'https://shanyraq.valis.kz/api/property.building?fields=id,name,kato&kato=г.Костанай' -H 'api_key: ВАШ-КЛЮЧ'

.. code:: JSON

    {"records":[{"id":225,"name":"мкр. Юбилейный 14","kato":[29868,"г.Костанай"]}, ... ],"length":28}


Если хотите исправить данные МЖД или Показание можете в запрос можно передавать ИД записи
Ниже пример исправление МЖД с Идентификатором 225 (property.building) с Названием улице Юбилейный 14/2


.. code:: bash

    curl -X PUT 'https://shanyraq.valis.kz/api/property.building/225/' -d '{"name":"Юбилейный 14/2"}' -H 'api_key: ВАШ-КЛЮЧ'

Ответ:

.. code:: JSON

    {"records":[{"id":225,"name":"мкр. Юбилейный 14", ...}],"length":1}
